package service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import model.Produit;
import repository.ProduitRepository;

@Service
@Transactional
public class ProduitService {
	
	@Autowired
	private ProduitRepository productRepository;
	
	@Transactional(readOnly = true)
	public List<Produit> getAllProducts() {
		return productRepository.findAll();
	}
	
	public void addProduct(Produit product) {
		productRepository.save(product);
	}
	
	public void deleteProduct(Long id) {
		productRepository.deleteById(id);
	}
}
