package repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;

import model.Produit;

public interface ProduitRepository extends JpaRepository<Produit, Long> {
	
	Produit findByNameAndPrice
		(
					@Param("nom") String nom,
					@Param("prix") double prix
		);	
}
