package producer;

import java.util.List;

import javax.annotation.PostConstruct;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import model.Produit;
import service.ProduitService;

@Component
public class ProduitProducer {
	private Log logger = LogFactory.getLog(ProduitProducer.class);
	private ProduitService productService;
	
	@Autowired
	public ProduitProducer(ProduitService productService) {
		this.productService = productService;
	}
	
	@PostConstruct
	public void produceData() {
		findProducts();
		addOneProduct();
		findProducts();
	}
	private void addOneProduct() {
		logger.info("-> Adding new product now!");
		productService.addProduct(new Produit("iPhone", 10));
	}
	
	private void findProducts() {
		logger.info("Trying to find all products.");
		List<Produit> allProducts = productService.getAllProducts();
		if(allProducts.isEmpty()) {
			logger.info("--No product found--");
		} else {
			for (Produit foundProduct : allProducts) {
				logger.info(String.format("product with id %d and name %s found :)",
				 foundProduct.getId(), foundProduct.getNom()));
			}
		}
	}
}
