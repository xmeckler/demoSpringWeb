package dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import model.Produit;
import util.Context;

public class DAOProduit {
	public void insert(Produit p) {
		EntityManager em = Context.getInstance().getEntityManagerFactory().createEntityManager();
		
		em.getTransaction().begin();
		em.persist(p);
		
		em.getTransaction().commit();
		em.close();
		Context.destroy();
	}
	
	public Produit selectById(int id) {
		EntityManager em = Context.getInstance().getEntityManagerFactory().createEntityManager();
		
		Produit p = em.find(Produit.class, id);
		System.out.println(p);
		
		em.close();
		Context.destroy();
		return p;
	}
	
	public void update(Produit p) {
		EntityManager em = Context.getInstance().getEntityManagerFactory().createEntityManager();
		
		em.getTransaction().begin();
		em.merge(p);
		
		em.getTransaction().commit();
		em.close();
		Context.destroy();
	}
	
	public void delete(Produit p) {
		EntityManager em = Context.getInstance().getEntityManagerFactory().createEntityManager();
		
		em.getTransaction().begin();
		
		em.remove(em.merge(p));
		
		em.getTransaction().commit();
		em.close();
		Context.destroy();
	}

	public List<Produit> selectAll() {
		EntityManager em = Context.getInstance().getEntityManagerFactory().createEntityManager();
		
		Query query = em.createQuery("SELECT a from Produit a");
		List<Produit> Produits = query.getResultList();
		
		em.close();
		Context.destroy();
		return Produits;
	}
}
