package repository;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.test.context.junit4.SpringRunner;

import static org.fest.assertions.Assertions.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment=WebEnvironment.RANDOM_PORT)
public class ProduitRepositoryTest {
	
	@Autowired
	private ProduitRepository productRepository;
	
	@Test
	public void testFindAll_andProducerWorked() {
		assertThat(productRepository.findAll()).hasSize(1);
	}
	
	@Test
	public void findByNameAndPrice_andNoneFound() {
		assertThat(productRepository.findByNameAndPrice("android", 10)).isNull();
	}
	
	@Test
	public void findByNameAndPrice_andFoundOne() {
		assertThat(productRepository.findByNameAndPrice("iPhone", 10)).isNotNull();
	}
}
